'''
    @file masher.py
    @author Alexander Ponomarev
    @date 12/04/18

'''

# Imports #
import argparse
import os
import subprocess
import shutil
import re
import collections
from datetime import datetime
from pprint import pprint

AVG_WORD_TIME_MS = 1200
MIN_WORD_TIME_MS = 450

MAX_WORDS = 15

# Functions #

def relative_file(filename):
    """Find a file that is relative to this python source file
        Args:
            filename: relative file path
        Returns:
            the absolute path to the filename
    """
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), filename)


def parse_params():
     # Parse command line arguments
    parser = argparse.ArgumentParser()

    parser.add_argument("--url",
                        required=True,
                        help="Youtube video id/url.")
                        
    parser.add_argument("--word",
                        required=True,
                        help="Word to match.")

    parsed_arguments = parser.parse_args()
    
    return parsed_arguments

def run_command(command):
    print command
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    #Try shell=True if the above doesn't work with shell=False
    p_stdout = p.stdout.read()
    p_stderr = p.stderr.read()
    p.communicate()
    
    if p.returncode != 0:
        print p_stderr
        return False
    
    return p_stdout
    
def youtube_dl(params):
    return run_command("./youtube-dl" + " " + params)
    
def clear_temp():
    shutil.rmtree('./temp', True)


'''
00:00:00.060 --> 00:00:02.030 align:start position:0%
 
mr.<00:00:00.539><c> Zuckerberg</c><00:00:00.719><c> does</c><c.colorE5E5E5><00:00:01.079><c> Facebook</c><00:00:01.650><c> consider</c></c>
'''
def to_mili(timestamp):
    splitted = timestamp.split(":")
    seconds_mili = splitted[2].split(".")
    
    hours = int(splitted[0])
    minutes = int(splitted[1])
    seconds = int(seconds_mili[0])
    mili = int(seconds_mili[1])
    return mili + (seconds * 1000) + (minutes * 60 * 1000) + (hours * 60 * 60 * 1000)

def create_word(word, start, end):
    return {'word': word, 'start': start, 'end': max(start + MIN_WORD_TIME_MS, min(start + AVG_WORD_TIME_MS, end))}

def read_vtt():
    match_start_pattern = re.compile(r"([0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9][0-9]) -->")
    match_time_pattern = re.compile(r"(.*?)<([0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9][0-9])>")
    match_time_end_pattern = re.compile(r"><c>([a-zA-Z .,]*?)</c></c>$")
    start = 0
    max_end_time = 0
    last_word = ""
    words = []

    with open('./temp/vid.en.vtt') as sub:
        lines = sub.readlines()
        for line in lines:
            match = match_start_pattern.match(line)
            if match != None:
                mili = to_mili(match.group(1))
                
                if start != 0 and last_word != "" and words[-1]['word'] != last_word:
                    words.append(create_word(last_word, start, mili))
                
                start = mili
                continue
            
            match = match_time_pattern.findall(line)
            if match != []:
                for wordtime in match:
                    word = wordtime[0].replace('</c>', '').replace('/', '').replace('.colorCCCCCC', '').replace('.colorE5E5E5', '').replace(' ', '').replace('<c>', '')
                    max_end_time = to_mili(wordtime[1])
                    words.append(create_word(word, start, max_end_time))
                    
                    start = max_end_time
                # Catch last word
                match = match_time_end_pattern.search(line)
                if match != None:
                    last_word = match.group(1).strip(' ')
                
    return words
                
def list_most_occuring(words_time, num):
    words = []
    for word_time in words_time:
        words.append(word_time['word'])
        
    counter = collections.Counter(words)
    for word in counter.most_common()[:num]:
        print str(word[1]) + " -> " + str(word[0])

def get_time_list_for_word(words_time, requested_word):
    durations_list = []
    for word_time in words_time:
        if word_time['word'] == requested_word:
            durations_list.append([word_time['start'] / 1000.0, (word_time['end'] - word_time['start']) / 1000.0])
            
            if (len(durations_list) >= MAX_WORDS):
                break
    
    return durations_list

def ffmpeg_slice_parts(durations_list):
    count = 1
    file_str = ""
    
    for duration in durations_list:
        run_command("ffmpeg -y -ss {} -t {} -i temp/vid.webm -strict -2 temp/part_{}.mp4".format(duration[0], duration[1], count))
        
        print "Slicing {}".format(count)
        file_str += "file 'part_{}.mp4'\r\n".format(count)
        count += 1
    
    with open('temp/list.txt', 'wb') as file:
        file.write(file_str)

def ffmpeg_concat():
    run_command("ffmpeg -y -f concat -i temp/list.txt -c copy output.mp4")

def main():
    clear = True
    args = parse_params()
    
    if (clear):
        # Clear temp directory
        clear_temp()
        
        # Upate youtube-dl
        youtube_dl("--update")
        
        if not run_command("ffmpeg -version"):
            print "Please install ffmpeg. sudo apt install ffmpeg"
            exit()
        
        # Fetch video transcription
        result = youtube_dl('-o "./temp/vid.%(ext)s" --format webm --write-auto-sub "' + args.url +'"')
        
        if "Writing video subtitles to" not in result: 
            print "File not found / no auto captions."
            exit()
        
    words_time = read_vtt()
    list_most_occuring(words_time, 30)
    durations = get_time_list_for_word(words_time, args.word)
    ffmpeg_slice_parts(durations)
    ffmpeg_concat()
    
    print "Done."

    

if __name__ == '__main__':
    main()
